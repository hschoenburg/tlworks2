<?php get_header(); ?>

  <?php 
    // get_template_part('./templates/title-tagline');
  ?>


  <div class="featured-image-container">
    <div id='blog' class="secondary-featured-image"></div>
  </div>




  <h1 class="hero"><?php the_title(); ?></h1>
  <div class="blog-post-container font-lastmile-gray">
    <?php
      $posts = get_posts('posts_per_page=20');
      foreach ($posts as $post): setup_postdata($post);
    ?>
    <div class="blog-post blog-hover">
      <a class='alternate' href="<?php echo get_the_permalink(); ?>">
        <div class="blog-image" style="background: url(<?php echo get_field('post_image'); ?>) no-repeat 0 50% / cover">
        </div>
        <div class='post-text'>          
          <h6 class="font-light-gray"><span><?php echo get_the_date(); ?> by <span class="author uppercase"><?php echo get_field('post_author'); ?></span></h6>
          <h1 class="post-title font-light-gray"><?php the_title(); ?></h1>
          <p class='' style='color: rgba(88,88,88,1);'><?php echo get_field('post_excerpt') ?></p>
        </div>
      </a>
    </div>

    <?php
      endforeach;
      wp_reset_postdata();
    ?>
  </div>

<?php get_footer(); ?>