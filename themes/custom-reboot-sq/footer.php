
  </div><!-- end container -->
  <footer class="bg-lastmile-gray font-light-gray">
    <div class="images">
      <?php
        $image1 = get_theme_mod('image1');
        $image2 = get_theme_mod('image2');
        $image3 = get_theme_mod('image3');
        $image4 = get_theme_mod('image4');
        $image5 = get_theme_mod('image5');

        if ($image1) {
            echo '<img class="image1 footer-images" src="'.$image1.'" />';
        }
        if ($image2) {
            echo '<img class="image2 footer-images" src="'.$image2.'" />';
        }
        if ($image3) {
            echo '<img class="image3 footer-images" src="'.$image3.'" />';
        }
        if ($image4) {
            echo '<img class="image4 footer-images" src="'.$image4.'" />';
        }
        if ($image5) {
            echo '<img class="image5 footer-images" src="'.$image5.'" />';
        }
      ?>
    </div>


    <div class="address">
      <?php 
        $company_name   = get_theme_mod('company_name');
        $address1       = get_theme_mod('address1');
        $address2       = get_theme_mod('address2');
        $city_state_zip = get_theme_mod('city_state_zip');
        $phone          = get_theme_mod('phone');
        $email          = get_theme_mod('email');

        if ($company_name) {
            echo '<div class="company-name avenir-black">'.$company_name.'</div>';
        }
        if ($address1) {
            echo '<div class="address1">'.$address1.'</div>';
        }
        if ($address2) {
            echo '<div class="address-2">'.$address2.'</div>';
        }
        if ($city_state_zip) {
            echo '<div class="city">'.$city_state_zip.'</div>';
        }
        if ($phone) {
            echo '<div class="phone">'.$phone.'</div>';
        }
        if ($email) {
            echo '<div class="email"><a href="mailto:'.$email.'">'.$email.'</a></div>';
        }
      ?>    
    </div>
    <?php
      if (is_front_page()) {
    ?>
            
    <script type="text/javascript" src='wp-content/themes/custom-reboot-sq/js/jquery.js'></script>
    <script type="text/javascript" src='wp-content/themes/custom-reboot-sq/scripts/main.js'></script>
      <?php
      }
      ?>
  </footer>



<?php wp_footer(); ?>
</body>
</html>