<?php get_header(); ?>

  <?php
    // get_template_part('./templates/title-tagline');
  ?>

  <div class="featured-image-container">
    <div class="secondary-featured-image thank-you"></div>
  </div>

  <h1 class="hero"><?php the_title(); ?></h1>
  <div class="section about-us-section fixed-width center">
    <div class="about-us-content font-light-gray link_adjust">
      <?php echo get_field('thank_you_content'); ?>
    </div>
  </div>

<?php get_footer(); ?>