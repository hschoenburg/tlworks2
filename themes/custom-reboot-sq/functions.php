<?php
/**
 * Custom functions and definitions
*/



// THESE ARE THE DEFAULTS FOR IMAGE SIZES IN THE
// SETTING > MEDIA SECTION IN THE WP DASHBOARD
// THIS WAS CHANGED TO STOP WP FROM ADDING
// ALL THE EXTRA IMAGES TO uploads/
// Thumbnail size  Width 150 Height 150
// Medium size Medium sizeMax Width 300 Max Height 300
// Large size  Large sizeMax Width 1024 Max Height 1024





// *
//  * Remove a new image size.
//  *
//  * @since 3.9.0
//  *
//  * @global array $_wp_additional_image_sizes
//  *
//  * @param string $name The image size to remove.
//  * @return bool True if the image size was successfully removed, false on failure.
 
// function remove_image_size( $name ) {
//   global $_wp_additional_image_sizes;

//   if ( isset( $_wp_additional_image_sizes[ $name ] ) ) {
//     unset( $_wp_additional_image_sizes[ $name ] );
//     return true;
//   }

//   return false;
// }




function custom_setup() {
  global $content_width;
  $content_width = 640;

  add_theme_support('post-thumbnails');
  add_theme_support('post-formats', array(
    'aside', 'image', 'video', 'quote', 'link', 'gallery'
  ));

  register_nav_menus( array(
    'primary' => __( 'Primary Menu' )
  ));
}
add_action('after_theme_setup', 'custom_setup');


function load_scripts() {
  // load style.css
  wp_enqueue_style('style', get_stylesheet_uri());
  // wp_enqueue_style('wow-style', get_stylesheet_directory_uri().'/styles/animate.min.css');

  // THIS IS FOR THE CYCLE/SLIDER
  // wp_register_script('jquery-cycle2', get_template_directory_uri() . '/scripts/jquery.cycle2.min.js');
  // wp_register_script('jquery-scrollVert', get_template_directory_uri() . '/scripts/jquery.cycle2.scrollVert.min.js');


  // LOAD SCRIPTS
  // THESE ARE NOW LOADED IN THE footer.php FILE DUE TO RENDER BLOCKING
  // wp_enqueue_script('jquery-2.1.1', get_template_directory_uri().'/js/jquery.js');
  // wp_enqueue_script('main', get_template_directory_uri().'/scripts/main.js');
  // wp_enqueue_script('wow', get_template_directory_uri().'/scripts/wow.min.js');
}
add_action('wp_enqueue_scripts', 'load_scripts');


// customizations to dashboard
function custom_dashboard($wp_customize) {
  $wp_customize->add_setting( 'logo', array(
    'sanitize_callback' => 'esc_url_raw',
    'transport' => 'postMessage'
  ));
  
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
    'label' => 'Logo',
    'section' => 'title_tagline',
    'settings' => 'logo',
    'priority' => 1,
  )));
}
add_action('customize_register', 'custom_dashboard');



// create a custom post type for project
add_action( 'init', 'create_project_type' );
function create_project_type() {
    $args = 
    array(
        'labels' => array(
            'name' => __( 'Project' ),
            'singular_name' => __( 'Project' ),
        ),
        'description' => 'Custom post type that displays in latest projects section.',
        'public' => true,
        'show_in_menu' =>  'edit.php',
        'has_archive' => true,
        'capability_type' => 'post',
        'heirarchical' => false,
        'supports' => array('title', 'author', 'editor', 'thumbnail', 'custom-fields', 'post-formats'  
        )   
    );
    register_post_type( 'project', $args );
};


// custom post type for testimonials
add_action( 'init', 'create_testimonial_type' );
function create_testimonial_type() {
    $args = 
    array(
        'labels' => array(
            'name' => __( 'Testimonial' ),
            'singular_name' => __( 'Testimonial' ),
        ),
        'description' => 'Custom post type that displays in latest projects section.',
        'public' => true,
        'show_in_menu' =>  'edit.php',
        'capability_type' => 'post',
        'heirarchical' => false,
        'supports' => array('title', 'author', 'editor', 'thumbnail', 'custom-fields', 'post-formats'  
        )   
    );
    register_post_type( 'testimonial', $args );
};


// customizing the footer
function register_footer($wp_customize) {   
  $wp_customize->add_panel('footer_panel', array(
    'priority'    => 100,
    'capability'  => 'edit_theme_options',
    'title'       => 'Footer'
  ));

  $wp_customize->add_section('footer_images', array(
    'title'       => 'Images',
    'description' => 'Add images',
    'priority'    => 100,
    'panel'       => 'footer_panel'
  ));

  $wp_customize->add_section('footer_address', array(
    'title'       => 'Address',
    'description' => 'Add an address',
    'priority'    => 200,
    'panel'       => 'footer_panel'
  ));

  $wp_customize->add_setting('image1');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image1', array(
    'label'    => 'Image 1',
    'section'  => 'footer_images'
  )));

  $wp_customize->add_setting('image2');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image2', array(
    'label'    => 'Image 2',
    'section'  => 'footer_images'
  )));

  $wp_customize->add_setting('image3');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image3', array(
    'label'    => 'Image 3',
    'section'  => 'footer_images'
  )));

  $wp_customize->add_setting('image4');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image4', array(
    'label'    => 'Image 4',
    'section'  => 'footer_images'
  )));

  $wp_customize->add_setting('image5');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image5', array(
    'label'    => 'Image 5',
    'section'  => 'footer_images'
  )));


  $wp_customize->add_setting('company_name');
  $wp_customize->add_control('company_name', array(
    'label'       => 'Company Name',
    'type'        => 'text',
    'section'     => 'footer_address'
  ));

  $wp_customize->add_setting('address1');
  $wp_customize->add_control('address1', array(
    'label'       => 'Address 1',
    'type'        => 'text',
    'section'     => 'footer_address'
  ));

  $wp_customize->add_setting('address2');
  $wp_customize->add_control('address2', array(
    'label'       => 'Address 2',
    'type'        => 'text',
    'section'     => 'footer_address'
  ));

  $wp_customize->add_setting('city_state_zip');
  $wp_customize->add_control('city_state_zip', array(
    'label'       => 'City, State, Zip',
    'type'        => 'text',
    'section'     => 'footer_address'
  ));

  $wp_customize->add_setting('phone');
  $wp_customize->add_control('phone', array(
    'label'       => 'Phone',
    'type'        => 'text',
    'section'     => 'footer_address'
  ));

  $wp_customize->add_setting('email');
  $wp_customize->add_control('email', array(
    'label'       => 'Email',
    'type'        => 'text',
    'section'     => 'footer_address'
  ));
}
add_action('customize_register', 'register_footer');


$frontPageViewedAlready = false;


?>



