<?php get_header(); ?>


	<div class="post-container font-lastmile-gray">
		<?php
		$posts = get_posts('showposts=10');
		foreach ($posts as $post): setup_postdata($post);
		?>
			<div class="post">
				<?php
				$image = get_field('post_image');
				if ($image) :
					echo '<img class="post-image" src="'.$image.'" />';
				endif;
				?>
				<div class="post-info">
					<h6 class="avenir-light font-light-gray">Posted <a href="<?php echo home_url(get_the_date('Y/m/d')); ?>" class="date uppercase font-blue"><?php echo get_the_date(); ?></a> by <a href="<?php the_permalink(the_author()); ?>" class="author uppercase font-blue"><?php the_author(); ?></a></h6>
					<a class="underline font-blue" href="<?php the_permalink(); ?>">
						<h1 class="post-title avenir-black"><?php the_title(); ?></h1>
						<p class="post-excerpt avenir-light font-light-gray"><?php echo get_field('post_excerpt'); ?></p>
					</a>
				</div>
			</div>
<?php
		endforeach;
		wp_reset_postdata();
		?>
	</div>









<?php get_footer(); ?>