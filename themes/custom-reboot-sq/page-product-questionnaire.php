<?php get_header(); ?>

  <?php 
     // get_template_part('./templates/title-tagline');
  ?>

  <div class="featured-image-container">
    <div class="secondary-featured-image questionnaire"></div>
  </div>

  <h1 class="hero" style='z-index: 9999999; line-height: 1; top: -.75em;'><?php the_title(); ?></h1>

    <!-- ================== FORM SECTION ================== -->
  <div class="section product-questionnaire-form-section">
    <?php echo get_field('product_questionnaire_form_info'); ?>
  </div>

<?php get_footer(); ?>