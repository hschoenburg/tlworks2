<?php get_header(); ?>

   <style type="text/css">
      .navbar {
         background: rgb(29,27,27);
      }
      .post-container {
         margin-top: 7rem;
      }
   </style>


   <div class="post-container project-post font-light-gray fixed-width center">
      <div class="post relative">
         <h1 class="post-title font-medium"><?php the_title(); ?></h1>
         <a class="live-link bg-light-blue" target="_blank" href="<?php echo get_field('project_link'); ?>">Live Demo</a>
         <?php
         $image = get_field('post_image');
         if ($image) :
            echo '<img class="post-image" src="'.$image.'" />';
         endif;
         ?>
         <div class="post-info">
            <?php echo get_field('project_text'); ?>
         </div>
      </div>
   </div>


<?php get_footer(); ?>