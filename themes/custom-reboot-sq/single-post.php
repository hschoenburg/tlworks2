<?php get_header(); ?>

   <style type="text/css">
      .navbar {
         background: rgb(29,27,27);
      }
      .post-container {
         margin-top: 7rem;
      }
   </style>

   <div class="post-container font-light-gray fixed-width center">
      <div class="post">
         <h1 class="post-title font-medium"><?php the_title(); ?></h1>
         <?php
         $image = get_field('post_image');
         if ($image) :
            echo '<img class="post-image" src="'.$image.'" />';
         endif;
         ?>
         <div class="post-info">
            <h6 class="avenir-light font-light-gray">Posted <?php echo get_the_date(); ?></a> by <span class="author uppercase"><?php echo get_field('post_author'); ?></span></h6>
            <p class="post-excerpt avenir-light font-light-gray"><?php echo get_field('post_text'); ?></p>
         </div>
      </div>
   </div>


<?php get_footer(); ?>