<?php /* Template Name: Default Template */ ?>

<?php get_header(); ?>

  <main id='main'>
    <div class="featured-section">
    <?php
      if ( !wp_is_mobile() && is_front_page() ) {
        get_template_part( 'templates/video' );
      }
      elseif ( wonderplugin_is_device('iPad') && is_front_page() ) {
        get_template_part( 'templates/video' );
      }
      else {  
    ?>
      <ul>
        <li class="image-replace index-img"></li>
      </ul>
    <?php
      }
    ?>

      <div class="tagline-container">
        <div class="tagline center">
          <h1 class="title tagline-1"><?php echo get_field('tag_line'); ?></h1>
          <h1 class="title tagline-2"><?php echo get_field('tag_line_2'); ?></h1>
          <a class="learn-more center page-down" href="#who-we-are-section"><p>Learn More</p></a>
        </div>
      </div>
    </div><!-- end .featured-section -->

    <div class="enquiry-section">
    <!-- <h1>Featured in these media publications:</h1> -->
      <?php
      if( have_rows('enquiries') ):
        while ( have_rows('enquiries') ) : the_row();
      ?>
        <div class="enquiry-block">
            <a target="_blank" href="<?php echo get_sub_field('enquiry_link'); ?>">
            <div class="enquiry-grayscale" style="background: url('<?php echo get_sub_field('enquiry_image'); ?>'); background-repeat: no-repeat; background-size: contain; background-position: 50%;"></div>
            <div class="enquiry-color" style="background: url('<?php echo get_sub_field('enquiry_image_color'); ?>'); background-repeat: no-repeat; background-size: contain; background-position: 50%;"></div>
            </a>
        </div>
          <?php 
        endwhile;
      endif;
      ?>
    </div> <!-- end .enquiry-block-container -->


    <div class="process-section center" id="who-we-are-section">
      <div class="section-container">
        <h1 class="title process-title"><?php echo get_field('process_title'); ?></h1>
        <div class="process-block-container sub-container">
        <?php
        $sliderObject = array(
          array('effect'=>'fadeInLeft', 'delay'=>'0s'), 
          array('effect'=>'fadeIn', 'delay'=>'1s'), 
          array('effect'=>'fadeInRight', 'delay'=>'0s')
        );
        $counter = 0;
        if( have_rows('process_block') ):
          while ( have_rows('process_block') ) : the_row();
        ?>
          <div class="process-block font-center">
              <img class="<?php echo $sliderObject[$counter]['effect']; ?>" src="<?php echo get_sub_field('process_image'); ?>"/ >
              <p class="font-larger font-title-case"><?php echo get_sub_field('process_block_title'); ?></p>
              <p class=""><?php echo get_sub_field('process_info'); ?></p>
          </div>
            <?php 
          endwhile;
        endif;
        ?>
        </div> <!-- end .process-block-container -->
      </div>
    </div> <!-- end .process-section -->

    
    <div id='slider' class="testimonials-section slider-section">
      <div class="overlay"></div>
      <ul class="testimonial-image-container un_list"> 

      <?php
        // ADDED THIS CONDITIONAL TO ADJUST FOR MOBILE DESIGN
        if (wp_is_mobile()) {
          $testimonials = get_posts('posts_per_page=1&post_type=testimonial');
        } else {
          // THE NUMBER OF 'posts_per_page' NEEDS TO CHANGE PER THE NUMBER OF TESTIMONIALS
          // MUST ALSO CHANGE THE z-index SETTINGS IN style.css
          $testimonials = get_posts('posts_per_page=3&post_type=testimonial');
        }
        if ($testimonials) :
          foreach ($testimonials as $post) :
            setup_postdata($post);
            ?>

            <li class="testimonial-image" style="background: url('<?php echo get_field('testimonial_image'); ?>')">
              <div class="testimony">
                <p class="testimonial font-white"><?php echo get_field('testimonial'); ?></p>
                <p class="testimonial-author font-white">
                  <?php echo get_field('testimonial_author'); ?>,
                    <?php echo get_field('testimonial_authors_title'); ?>
                </p>
              </div>
            </li>

            <?php
          endforeach;
        endif;
        wp_reset_postdata();
      ?>
      </ul>
            <div class="testimonial-chevron-icons">
              <div class="icon icon-left" data-direction-left="true">&#xf137;</div>
              <div class="icon icon-right" data-direction-left="false">&#xf138;</div>
            </div>
    </div>


    <div class="our-process-section process-section center" id="process-section">
      <div class="section-container">
        <h1 class="title process-title"><?php echo get_field('process_title_2'); ?></h1>
        <div class="process-block-container sub-container">
        <?php
        $counter = 0;
        if( have_rows('process_block_2') ):
          while ( have_rows('process_block_2') ) : the_row();
        ?>
          <div class="process-block font-center">
              <img src="<?php echo get_sub_field('process_image'); ?>" / >
              <p class="font-larger font-title-case"><?php echo get_sub_field('process_block_title'); ?></p>
              <p class="line-adjust"><?php echo get_sub_field('process_info'); ?></p>
          </div>
            <?php 
          endwhile;
        endif;
        ?>
        </div> <!-- end .process-block-container -->
      </div>
    </div> <!-- end .process-section -->


    <div class="testimonials-section project-section code_sample">
      <div class="overlay"></div>
      <div class=""> 
        <div class="site">
        </div>
          <a class="learn-more center" href="<?php echo get_page_uri(112); ?>">View our work</a>
      </div>
    </div>

  </main>

<?php get_footer(); ?>
