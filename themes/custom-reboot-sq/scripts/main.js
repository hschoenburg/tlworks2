$(function() {


    // THIS WAS TAKEN OUT OF THE index.php PAGE
    // reduce logos size on scroll
    // var $logo = $('div.logo img');
    // if (window.pageYOffset === 0 ) {
    //    $logo.css({
    //       width: 380, 
    //    });
    //    $(window).on('scroll', function(e) {
    //       $logo.animate({width: 230, marginLeft: 20}, 500);
    //    })
    // };

    // // resize the logo on any click 
    // $(window).on('click', function() {
    //    $logo.animate({width: 230, marginLeft: 20}, 500);
    // });









  /* ====================CLICK EVENTS======================*/

  // display contact-us form modal on front-page and modify its css props
  // open
  // $('.page-item-108').on('click', function(e) {
  //   e.preventDefault();
  //   $('#mynav-check').prop('checked', false);
  //   $('.contact-us-form-section').slideToggle(500);
  // });
  // //close
  // $('.close-modal').on('click', function(e) {
  //   e.preventDefault();
  //   $('#mynav-check').prop('checked', false);
  //   $('.contact-us-form-section').slideToggle(500);
  // });


  // scroll transition
  $('.page-down, .page-up').on('click', function(e) {
    e.preventDefault();
    var $id = e.currentTarget.hash;
    $('html, body').animate({scrollTop: $($id).offset().top - 95}, 1500);
  });


  // carousel for testimonials
  $('.icon').on('click', function(e) {
    var $direction = e.currentTarget.dataset.directionLeft;

    $('.testimonial-image:first-child').animate({
      left: $direction == "true" ? -$(window).innerWidth() : $(window).innerWidth()
    },
    750,
    'swing',
    function() {
      $(this).remove().css({'left': 0}).appendTo('.testimonial-image-container');
    });

  });




    /* ====================SCROLL EVENTS======================*/
    // var fired = false;
    // $(window).on('scroll', function(e) {
    //   if (!fired) {

    //     if (e.currentTarget.pageYOffset >= 30) {
    //       $('.navbar').addClass('change-navbar-bg-color');
    //     } else {
    //       $('.navbar').removeClass('change-navbar-bg-color')
    //     }
        

    //     fired = true;
    //   }


    //   setTimeout(function() {
    //     fired = false;
    //   }, 200)
    // });




    /* =======================================================*/

    // // initialize wow
    // var wow = new WOW(
    //   {
    //     boxClass:     'wow',      // animated element css class (default is wow)
    //     animateClass: 'animated', // animation css class (default is animated)
    //     offset:       200,
    //     mobile:       false,       // trigger animations on mobile devices (default is true)
    //     live:         true,       // act on asynchronously loaded content (default is true)
    //     callback:     function(box) {
    //       // the callback is fired every time an animation is started
    //       // the argument that is passed in is the DOM node being animated
    //       var transformOrigin = box.getAttribute('data-transform-origin');
    //       if (transformOrigin) {
    //          box.style.webkitTransformOrigin = transformOrigin;
    //          box.style.msTransformOrigin = transformOrigin;
    //          box.style.transformOrigin = transformOrigin;
    //       }
    //     },
    //     scrollContainer: null // optional scroll container selector, otherwise use window
    //   }
    // );
    // wow.init();





});

