<!-- blog posts sidebar -->
<div class="project-sidebar blog-sidebar">
	<p class="latest-posts">Most recent projects:</p>
	<?php
		$blog_posts = get_posts('post_type=project&showposts=10&exclude='.$post->ID);

		foreach ($blog_posts as $post) :
			setup_postdata($post);
		?>
			<a class="project-container blog-container" href="<?php the_permalink(); ?>">
				<div class="blog-photo">
					<img src="<?php echo get_field('project_image'); ?>" />
				</div>
				<span class="project-post-title"><?php echo get_field('project_title'); ?></span>
				<p class="project-post"><?php echo get_field('project_intro'); ?></p>
			</a>

		<?php
		endforeach;
		wp_reset_postdata();

	?>
</div>