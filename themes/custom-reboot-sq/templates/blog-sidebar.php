<!-- blog posts sidebar -->
<div class="blog-sidebar">
	<p class="latest-posts">Mosts recents posts:</p>
	<?php
		$blog_posts = get_posts('category=blog-posts&showposts=10&exclude='.$post->ID);

		foreach ($blog_posts as $post) :
			setup_postdata($post);
		?>
			<a class="blog-container" href="<?php the_permalink(); ?>">
				<div class="blog-photo">
					<img src="<?php echo get_field('post_image'); ?>" />
				</div>
				<p class="blog-post-title"><?php the_title(); ?></p>
			</a>

		<?php
		endforeach;
		wp_reset_postdata();

	?>
</div>