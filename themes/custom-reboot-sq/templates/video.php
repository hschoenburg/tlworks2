<ul>
  <li>
      <?php
        if ($_SERVER['WP_ENV'] == 'production') {
      ?>
          <video id=hide-video preload="metadata" poster="<?php echo get_field('featured_image_1'); ?>" autostart autoplay loop>
            <source src="https://tlmworks-storage-prod.s3-us-west-1.amazonaws.com/TheLastMileWebClips.mp4" /> 
          </video>
      <?php
        } else if ($_SERVER['WP_ENV'] == 'development') {
      ?>
          <video id=hide-video preload="metadata" poster="<?php echo get_field('featured_image_1'); ?>" autostart autoplay loop>
            <source src="<?php echo bloginfo('stylesheet_directory').'/videos/the_last_mile_web_clips.mp4'; ?>" type="video/mp4" />
          </video>
      <?php
        }
      ?>
  </li>
</ul>