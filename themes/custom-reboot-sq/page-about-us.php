<?php get_header(); ?>

  <!-- FEATURED IMAGE WITH TITLE -->
  <div class="featured-image-container">
    <div class="secondary-featured-image about-us"></div>
  </div>

  <h1 class="hero"><?php the_title(); ?></h1>
	<div class="section about-us-section fixed-width center">
		<div class="about-us-content font-light-gray">
			<?php echo get_field('about_us_content'); ?>
		</div>
	</div>

<?php get_footer(); ?>