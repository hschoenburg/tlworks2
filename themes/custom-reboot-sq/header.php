<?php
/**
 * The Header for our theme.
 * Displays all of the <head> section and everything up till <div id="content">
 */
?>


<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <title><?php bloginfo('title'); ?></title>

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <!-- FAVICON FOR TAB IN BROWSER -->
  <link href="./wp-content/themes/custom-reboot-sq/images/favicon_v_2.ico" rel="icon" type="image/x-icon" />


  <!-- THIS IS SO THE CONTACT FORM WON'T ERROR: jQuery NOT DEFINED -->
<?php
  if (is_page('contact-us')) {
    ?>
  <script type="text/javascript" src='../wp-content/themes/custom-reboot-sq/js/jquery.js'></script>
<?php
  }
?>

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
  <nav class="navbar">
    <div class="navbar-container">
      <div class="logo">
        <a href="<?php echo get_home_url(); ?>">
        <?php 
          $logo = get_theme_mod('logo');

          if (isset($logo) && $logo != "") :
            echo '<img class="nav_logo" src="'.esc_url( $logo ).'" alt="'.esc_attr(get_bloginfo('title')).'">';

          endif;
        ?>
        </a>
      </div>
      <input id="mynav-check" class="mynav-check" type="checkbox"></input>
      <label class="mynav-label" for="mynav-check">Navigate</label>
      <?php 
      // $page_to_exclude = 
        wp_nav_menu();
      ?>
    </div>
  </nav>

  <div class="container">

