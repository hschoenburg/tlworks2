<?php	get_header(); ?>

  <?php 
     // get_template_part('./templates/title-tagline');
  ?>


  <div class="featured-image-container">
    <div class="secondary-featured-image contact"></div>
  </div>

  <h1 class="hero contact-heading" style='z-index: 9999999;'><?php the_title(); ?></h1>
  <div class="blog-post-container font-lastmile-gray">
    <div class='blog-post no-hover-class' id='contact_adjust'>
        <h3><?php echo get_field('question'); ?></h3>
        <div class="contact-us-content font-light-gray">
          <?php echo get_field('contact_us_form_info'); ?>
        </div>
    </div>
    <?php wp_reset_postdata(); ?>
  </div>

    <!-- ================== FORM SECTION ================== -->
  <div class="section product-questionnaire-form-section">
    <?php echo get_field('product_questionnaire_info'); ?>
  </div>

<?php get_footer(); ?>