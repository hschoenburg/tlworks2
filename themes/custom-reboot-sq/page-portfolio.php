<?php	get_header(); ?>

   <?php 
      // get_template_part('./templates/title-tagline'); 
   ?>



  <div class="featured-image-container">
    <div class="secondary-featured-image portfolio"></div>
  </div>






  <h1 class="hero"><?php the_title(); ?></h1>
  <div class="blog-post-container font-lastmile-gray">
    <?php
    $posts = get_posts('post_type=project&posts_per_page=20');
    foreach ($posts as $post): setup_postdata($post);
    ?>
    <div class="blog-post blog-hover">
      <a class='alternate' href="<?php echo get_the_permalink(); ?>">
        <div class="blog-image" style="background: url(<?php echo get_field('project_image'); ?>); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat">
        </div>
        <div class="post-text">
          <h6 class="font-light-gray"><span><?php echo get_the_date(); ?></span></h6>
          <h1 class="post-title font-light-gray"><?php the_title(); ?></h1>
          <h3 class="project-intro font-light-gray"><?php echo get_field('project_intro'); ?></h3>
          <p class="project-intro font-light-gray"><?php echo get_field('project_excerpt'); ?></p>
        </div>
      </a>
    </div>

    <?php
    endforeach;
    wp_reset_postdata();
    ?>
  </div>

<?php get_footer(); ?>